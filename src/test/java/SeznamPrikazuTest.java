package test.java;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import main.java.logika.*;


/*******************************************************************************
 * Testovací třída SeznamPrikazuTest slouží ke komplexnímu otestování třídy  
 * SeznamPrikazu
 * 
 * @author    Luboš Pavlíček
 * @version   pro školní rok 2016/2017
 */
public class SeznamPrikazuTest
{
    private Hra hra;
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;
    private PrikazSeber prSeber;
    private PrikazObsahBatohu prObsBat;
    private PrikazMluvit prMluvit;
    private PrikazKombinovat prKombin;
    
    @Before
    public void setUp() {
        hra = new Hra();
        prKonec = new PrikazKonec(hra);
        prJdi = new PrikazJdi(hra.getHerniPlan(),hra);
        prSeber = new PrikazSeber(hra.getHerniPlan());
        prObsBat = new PrikazObsahBatohu(hra.getHerniPlan());
        prMluvit = new PrikazMluvit(hra.getHerniPlan(),hra);
        prKombin = new PrikazKombinovat(hra.getHerniPlan());
    }

    @Test
    public void testVlozeniVybrani() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prObsBat);
        seznPrikazu.vlozPrikaz(prMluvit);
        seznPrikazu.vlozPrikaz(prKombin);
        assertEquals(prKonec, seznPrikazu.vratPrikaz("konec"));
        assertEquals(prJdi, seznPrikazu.vratPrikaz("jdi"));
        assertEquals(null, seznPrikazu.vratPrikaz("nápověda"));
        assertEquals(prSeber, seznPrikazu.vratPrikaz("seber"));
        assertEquals(prObsBat, seznPrikazu.vratPrikaz("obsah_batohu"));
        assertEquals(prMluvit, seznPrikazu.vratPrikaz("mluvit"));
        assertEquals(prKombin, seznPrikazu.vratPrikaz("kombinovat"));
    }
    @Test
    public void testJePlatnyPrikaz() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prObsBat);
        seznPrikazu.vlozPrikaz(prMluvit);
        seznPrikazu.vlozPrikaz(prKombin);
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("konec"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("jdi"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("nápověda"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Konec"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("seber"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("obsah_batohu"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("mluvit"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("kombinovat"));
    }
    
    @Test
    public void testNazvyPrikazu() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prObsBat);
        seznPrikazu.vlozPrikaz(prMluvit);
        seznPrikazu.vlozPrikaz(prKombin);
        String nazvy = seznPrikazu.vratNazvyPrikazu();
        assertEquals(true, nazvy.contains("konec"));
        assertEquals(true, nazvy.contains("jdi"));
        assertEquals(false, nazvy.contains("nápověda"));
        assertEquals(true, nazvy.contains("seber"));
        assertEquals(true, nazvy.contains("obsah_batohu"));
        assertEquals(true, nazvy.contains("mluvit"));
        assertEquals(true, nazvy.contains("kombinovat"));
    }
    
}

/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package main.java.logika;





/*******************************************************************************
 * Instance interfejsu {@code IRodic} představují ...
 * The {@code IRodic} interface instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class Rodic
{
    private String kdo;
    private boolean splnenKvest;
    private String uvitani;
    private String textKvestNeSplnen;
    private String textKvestSplnen;
    private int krat;
    
    public Rodic(String kdo,String uvitani, String textKvestNeSplnen, String textKvestSplnen){
        this.kdo = kdo;
        this.uvitani = uvitani;
        this.splnenKvest = false;
        this.textKvestNeSplnen = textKvestNeSplnen;
        this.textKvestSplnen = textKvestSplnen;
        krat = 0;
    }

    public boolean isSplnenKvest(){
        return splnenKvest;
    }
    
    public void setSplnenKvest(boolean splnenKvest){
        this.splnenKvest = splnenKvest;
    }
    
    public String getTextKvestSplnen(){
        return textKvestSplnen;
    }
    
    public String getTextKvestNeSplnen(){
        krat++;
        return krat < 2? uvitani + textKvestNeSplnen : textKvestNeSplnen;
    }
    
    public String getKdo(){
        return kdo;
    }
}

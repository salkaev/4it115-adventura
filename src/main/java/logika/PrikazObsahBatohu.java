/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package main.java.logika;





/*******************************************************************************
 * Instance třídy {@code PrikazBatoh} představují ...
 * The {@code PrikazBatoh} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class PrikazObsahBatohu implements IPrikaz
{
    private HerniPlan plan;
    private static final String NAZEV = "obsah_batohu";
    
    public PrikazObsahBatohu(HerniPlan plan)
    {
        this.plan = plan;
    }
    
    @Override
    public String provedPrikaz(String... parametry) {
        return plan.getBatoh().getSeznam();
    }
    @Override
    public String getNazev() {
        return NAZEV;
    }
    



}

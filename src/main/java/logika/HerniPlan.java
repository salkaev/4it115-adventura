package main.java.logika;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    private Batoh batoh;
    private Otaznik otaznik;
    private Vec klic;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor vlastniPokoj = new Prostor("vlastní_pokoj","vlastní pokoj, ve kterem bydlite");
        Prostor obyvaciPokoj = new Prostor("obývací_pokoj", "obývací pokoj , ve kterem je gauč, televize a tatinek");
        Prostor pokojRodicu = new Prostor("pokoj_rodičů","pokoj rodičů, ve kterem maminka čte knižku");
        Prostor kuchyn = new Prostor("kuchyn","kuchyn, ve kterem je lednicka, linka se zboži a umyvadlo");
        Prostor chodba = new Prostor("chodba","chodba, ve kterem je skřiń a vychod ven");
        Prostor vychod = new Prostor("vychod","");
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        vlastniPokoj.setVychod(obyvaciPokoj);
        obyvaciPokoj.setVychod(vlastniPokoj);
        obyvaciPokoj.setVychod(kuchyn);
        obyvaciPokoj.setVychod(chodba);
        obyvaciPokoj.setVychod(pokojRodicu);
        kuchyn.setVychod(obyvaciPokoj);
        pokojRodicu.setVychod(obyvaciPokoj);
        chodba.setVychod(obyvaciPokoj);
        chodba.setVychod(vychod);
        
        //Vytvareni batohu
        batoh = new Batoh();
        
        //Vytvareni veci
        Vec chleba = new Vec("chleba", true, true);
        kuchyn.pridatVecVProstor(chleba);
        
        Vec klobasa = new Vec("klobasa",true, true);
        kuchyn.pridatVecVProstor(klobasa);
        
        klic = new Vec("klič", true, false);
        chodba.pridatVecVProstor(klic);
        
        //Vytvářeji se rodiči
        Rodic tatinek = new Rodic("tatinek","Ahoj, vidím, že chceš jit do party,a potřebuješ penize\n",//Uvitani
                                            "Udělaš mi chleba s klobsou a dostaneš 500 kč, chleba s klobasou mužeš najit v kuchyně\n"+
                                            "Aby udělat chleba s klobasou seber chleba a klobasu, a kombinuj jich.",//textKvestNeSplnen
                                             "Dikuju moc za chleba s klobasou, dostal jsi penize a mužeš jit do party\n"+
                                             "Zeptej se maminku, kde jsou klíče.");//textKvestSplnen
        obyvaciPokoj.setRodic(tatinek);
        
        Rodic maminka = new Rodic("maminka","Čau, jako vždycky, kde jsou klíče?!\n" ,//Uvitani
                                            "Odpověd' na tři otazky, potom tě řeknu kde je",//textKvestNeSplnen
                                            "Výborně , klice je na chodbě, na penize se ptej tatinka.");//textKvestSplnen
        pokojRodicu.setRodic(maminka);
        
        
        // vytvařeji se otaznik a otazky na kvest maminky
        otaznik = new Otaznik();
        otaznik.pridatOtazku(new Otazka("Kolik elektronů je v atomu vodíku?","1"));
        otaznik.pridatOtazku(new Otazka("2 + 2 * 2 =","6"));
        otaznik.pridatOtazku(new Otazka("Hlavní město Ruska","Moskva"));
        otaznik.pridatOtazku(new Otazka("Kolik planet je ve sluneční soustavě?","9"));
        otaznik.pridatOtazku(new Otazka("Kolik přikázání v Bibli?","9"));
        otaznik.pridatOtazku(new Otazka("V jakém roce začala první světová válka?","1914"));
        otaznik.pridatOtazku(new Otazka("Kolik stupňů potřebuje k zahřátí vody?","100"));
        otaznik.pridatOtazku(new Otazka("Jakeho typu je uvedena konstanta: 5f","float"));
        
        
        
        
        
                
        aktualniProstor = vlastniPokoj;  // hra začíná v domečku       
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }
    
    public Batoh getBatoh() {
        return this.batoh;
    }
    
    public Otaznik getOtaznik() {
        return otaznik;
    }
    
    public void klicViditelny() {
        klic.setViditelna(true);
    }
}

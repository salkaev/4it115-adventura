/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package main.java.logika;





/*******************************************************************************
 * Instance třídy {@code PrikazMluvit} představují ...
 * The {@code PrikazMluvit} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class PrikazMluvit implements IPrikaz
{
    private String NAZEV = "mluvit";
    private HerniPlan plan;
    private Hra hra;

    /***************************************************************************
     */
    public PrikazMluvit(HerniPlan plan,Hra hra)
    {
        this.plan = plan;
        this.hra = hra;
    }
    @Override
        public String provedPrikaz(String... parametry) {
            if (parametry.length == 0) {
                // pokud chybí druhé slovo (sousední prostor), tak ....
                return "S kim musim mluvit? Musíš zadat s kim";
            }
    
            String skim = parametry[0];
            Rodic rodic = plan.getAktualniProstor().getRodic();
            
            
            if(rodic.getKdo().equals(skim)){
                if((skim.equals("tatinek")&&(plan.getBatoh().jeVBatohu("chleba_s_klobasou")))){
                    rodic.setSplnenKvest(true);
                    hra.setPenize(true);
                }
                if(skim.equals("maminka")){
                    System.out.println(rodic.getTextKvestNeSplnen());
                    if (plan.getOtaznik().hraOtaznik()) {
                        rodic.setSplnenKvest(true);
                        plan.klicViditelny();
                    }
                }
                return rodic.isSplnenKvest() ? rodic.getTextKvestSplnen() : rodic.getTextKvestNeSplnen();
            }else{
                return skim + " neni tu";
            }
            
    
            
        }
        
    @Override
    public String getNazev() {
        return NAZEV;
    }


}

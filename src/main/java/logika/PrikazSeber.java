/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package main.java.logika;





/*******************************************************************************
 * Instance třídy {@code PrikazVzit} představují ...
 * The {@code PrikazVzit} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class PrikazSeber implements IPrikaz
{
    private static final String NAZEV = "seber";
    private HerniPlan plan;
    
    /***************************************************************************
     */
    public PrikazSeber(HerniPlan plan)
    {
        this.plan = plan;
    }
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Co musím vzit? Musíš zadat jméno veci";
        }
        
        String vec = parametry[0];
        
        Prostor prostor = plan.getAktualniProstor();
        Batoh batoh = plan.getBatoh();
        if((prostor.jeVecVProstoru(vec))&&(prostor.getVec(vec).isViditelna())){
            return batoh.pridatVec(prostor.odebratVec(vec));
        }else{
            return "V tomhle prostoru ne vidim " + vec + ".";
        }
        
    }
    @Override
    public String getNazev() {
        return NAZEV;
    }
}

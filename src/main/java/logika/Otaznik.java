/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package main.java.logika;


import java.util.Scanner;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;




/*******************************************************************************
 * Instance třídy {@code Otaznik} představují ...
 * The {@code Otaznik} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class Otaznik
{
    private ArrayList<Otazka> seznamOtazek;

    /***************************************************************************
     */
    public Otaznik()
    {
        seznamOtazek = new ArrayList<>();
    }

    public boolean hraOtaznik(){
        int nahcis;
        int pokus = 3;
        int spravne = 0;
        ArrayList<Otazka> otaznikTemp = (ArrayList<Otazka>)seznamOtazek.clone();
        System.out.println("Maš tři možnosti udělat chybu");
        while((pokus != 0)&&(spravne < 3)){
            nahcis = getNahodneCislo(0,otaznikTemp.size() - 1);
            Otazka otazka = otaznikTemp.get(nahcis);
            otaznikTemp.remove(nahcis);
            
            System.out.println("Otazka: " + otazka.getOtazka());
            
            String radek = prectiString();
            
            if (radek.equals(otazka.getOdpoved())) {
                System.out.println("Spravně!!!");
                spravne++;
            } else {
                pokus--;
                System.out.println("Ne, zkus to zase! Maš " + pokus + " pokusu");
            }
        }
        
        if (pokus == 0) {
            System.out.println("Špatně viš školní ukoly. Mužeš zkusit to zase");
            return false;
        } else {
            System.out.println("Dobře, odpovědel jsi na 3 otazky");
            return true;
        }
    }
    
    private String prectiString() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Odpověd > ");
        return scanner.nextLine();
    }
    
    public void pridatOtazku(Otazka otazka) {
        seznamOtazek.add(otazka);
    }
    
    private int getNahodneCislo(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    

   

}

/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package main.java.logika;

import java.util.Objects;





/*******************************************************************************
 * Instance třídy {@code Otazka} představují ...
 * The {@code Otazka} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class Otazka
{
    private String otazka;
    private String odpoved;

    /***************************************************************************
     */
    public Otazka(String otazka, String odpoved)
    {
        this.otazka = otazka;
        this.odpoved = odpoved;
    }
    
    public String getOtazka() {
        return otazka;
    }
    
    public String getOdpoved() {
        return odpoved;
    }
    
    @Override
    public int hashCode(){
        return Objects.hash(otazka,odpoved);
    }
    @Override
    public boolean equals(Object o){
        if (o == this) {
            return true;
        }
        
        if (!(o instanceof Otazka)) {
            return false;
        }
        Otazka v = (Otazka) o;
        
        return this.otazka == v.otazka 
            && this.odpoved == v.odpoved;
    }



}

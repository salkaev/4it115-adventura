/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package main.java.logika;





/*******************************************************************************
 * Instance třídy {@code PrikazKombinovat} představují ...
 * The {@code PrikazKombinovat} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class PrikazKombinovat implements IPrikaz
{
    private String NAZEV = "kombinovat";
    private HerniPlan plan;


    /***************************************************************************
     */
    public PrikazKombinovat(HerniPlan plan)
    {
        this.plan = plan;
    }
    @Override
    public String provedPrikaz(String... parametry){
        if (parametry.length == 0) {
            return "Co kombinovat? Musíš zadat veci.";
        }
        
        String vec1 = parametry[0];
        String vec2 = (parametry.length>1) ? parametry[1]: "";
        Batoh batoh = plan.getBatoh();
        if((vec1.equals("chleba")&&vec2.equals("klobasa"))||(vec1.equals("klobasa")&&vec2.equals("chleba"))){
            if (batoh.jeVBatohu(vec1)&&batoh.jeVBatohu(vec2)){
                batoh.odebratVec("chleba");
                batoh.odebratVec("klobasa");
                return batoh.pridatVec(new Vec("chleba_s_klobasou",true,true));
            }else{
                return "Nemate" + (!batoh.jeVBatohu(vec1) ?" " + vec1 : "") + (!batoh.jeVBatohu(vec2) ?" " + vec2 : "");
            }
            
        }else{
            return "Nevim jak to kombinovat";
        }
        
    }
    
    @Override
    public String getNazev(){
        return NAZEV;
    }



}

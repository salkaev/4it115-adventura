/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package main.java.logika;

import java.util.Objects;




/*******************************************************************************
 * Instance třídy {@code Vec} představují ...
 * The {@code Vec} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class Vec
{
//\CC== CONSTANT CLASS (STATIC) ATTRIBUTES (FIELDS) ============================
//\CV== VARIABLE CLASS (STATIC) ATTRIBUTES (FIELDS) ============================
private String nazevVeci;
private boolean prenositelna;
private boolean viditelna;


//##############################################################################
//\CI== STATIC INITIALIZER (CLASS CONSTRUCTOR) =================================
//\CG== CLASS (STATIC) GETTERS AND SETTERS =====================================
//\CM== OTHER NON-PRIVATE CLASS (STATIC) METHODS ===============================
//\CP== PRIVATE AND AUXILIARY CLASS (STATIC) METHODS ===========================



//##############################################################################
//\IC== CONSTANT INSTANCE ATTRIBUTES (FIELDS) ==================================
//\IV== VARIABLE INSTANCE ATTRIBUTES (FIELDS) ==================================

//##############################################################################
//\II== CONSTRUCTORS AND FACTORY METHODS =======================================

    /***************************************************************************
     */
    public Vec(String nazevVeci, boolean prenositelna, boolean viditelna)
    {
        this.nazevVeci = nazevVeci;
        this.prenositelna = prenositelna;
        this.viditelna = viditelna;
    }

public String getNazevVeci(){
    return this.nazevVeci;
}

public void setNazevVeci(String nazevVeci){
    this.nazevVeci = nazevVeci;
}

public boolean isPrenositelna(){
    return this.prenositelna;
}

public void setPrenositelna(boolean prenostitelna){
    this.prenositelna = prenostitelna;
}

public boolean isViditelna(){
    return this.viditelna;
}

public void setViditelna(boolean viditelna){
    this.viditelna = viditelna;
}


@Override
public int hashCode(){
    return Objects.hash(nazevVeci);
}
@Override
public boolean equals(Object o){
    if (o == this) {
        return true;
    }
    
    if (!(o instanceof Vec)) {
        return false;
    }
    Vec v = (Vec) o;
    
    return this.nazevVeci == v.nazevVeci;
}


//\IA== ABSTRACT METHODS =======================================================
//\IG== INSTANCE GETTERS AND SETTERS ===========================================
//\IM== OTHER NON-PRIVATE INSTANCE METHODS =====================================
//\IP== PRIVATE AND AUXILIARY INSTANCE METHODS =================================



//##############################################################################
//\NT== NESTED DATA TYPES ======================================================
}

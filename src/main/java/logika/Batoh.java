/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package main.java.logika;

import java.util.Map;
import java.util.HashMap;




/*******************************************************************************
 * Instance třídy {@code Batoh} představují ...
 * The {@code Batoh} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class Batoh
{
    private Map<String,Vec> veciVBatohu;
    private final int MAX_VELIKOST = 4;
    /***************************************************************************
     */
    public Batoh()
    {
        veciVBatohu = new HashMap<>();
        
    }
    
    public String pridatVec(Vec vec){
        if(MAX_VELIKOST == veciVBatohu.size()){
            return "V batohu neni mista";
        }else{
            veciVBatohu.put(vec.getNazevVeci(),vec); 
            return "Uložil jsu do batohu " + vec.getNazevVeci();
        }
    }
    
    public boolean jeVBatohu(String vec){
        return veciVBatohu.containsKey(vec);
    }
    
    public Vec odebratVec(String vec){
        return veciVBatohu.remove(vec);
    }
    
    public String getSeznam(){
        String seznam ="";
        for(Vec vec: veciVBatohu.values()){
            seznam += seznam == "" ? " " + vec.getNazevVeci(): ", " + vec.getNazevVeci();
        }
        return "V batohu je:" + (seznam.equals("")? "žadna vec v batohu" :" " + seznam);
    }

}
